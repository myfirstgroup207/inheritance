﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceTask
{
    //TODO: Create public class 'Manager' here, which inherits from 'Employee' class
    public class Manager : Employee
    {
        //TODO: Define private integer field: 'quantity'
        private readonly int quantity;

        //TODO: Define constructor with three parameters: 'name'(string), 'salary'(decimal) and 'clientAmount'(int). Assign two first parameters to base class.
        public Manager(string name, decimal salary, int clientAmount):base(name, salary)
        {
            quantity = clientAmount;
        }

        //TODO: Override public virtual method 'SetBonus', which increases bonus depending on clients amount

        public override void SetBonus(decimal bonus)
        {
            if (quantity>150)
            {
                base.SetBonus(bonus+1000);
            }
            else if(quantity > 100)
            {
                base.SetBonus(bonus+500);
            }
            else
            {
                base.SetBonus(bonus);
            }
        }
    }
}

