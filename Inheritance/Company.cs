﻿using System;
using System.Collections.Generic;
using System.Text;


namespace InheritanceTask
{
    //TODO: Create public class 'Company' here
    public class Company
    {
        //TODO: Define private field that is array of employees: 'employees'
        private readonly Employee[] employees;
        //TODO: Define constructor that gets array of employees, and assign them to its field
        public Company(Employee[] employees)=> this.employees = employees;

        //TODO: Define public method 'GiveEverbodyBonus' with parameter 'companyBonus', that set basic bonus to every employee in the company
        public void GiveEverbodyBonus(decimal companyBonus)
        {
            for(int i=0;i<employees.Length;i++)
            {
                employees[i].SetBonus(companyBonus);
            }
        }

        //TODO: Define public method 'TotalToPay', that returns the total salary + bonus of all employees of the company 
        public decimal[] TotalToPay()
        {
            decimal[] allSal = new decimal[employees.Length];
            for(int i=0;i<allSal.Length;i++)
            {
                allSal[i] = employees[i].ToPay();
            }
            return allSal;
        }
        //TODO: Define public method 'NameMaxSalary', that returns emloyee’s name who has maximum salary + bonus in the company 
        public string NameMaxSalary()
        {
            string name = employees[0].Name;
            decimal sal = employees[0].Salary;
            for(int i = 1; i < employees.Length; i++)
            {
                if (employees[i].Salary > sal)
                {
                    sal = employees[i].Salary;
                    name = employees[i].Name;
                }
            }
            return name;
        }
    }
}
