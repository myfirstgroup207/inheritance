﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceTask
{
    //TODO: Create public class 'SalesPerson' here, which inherits from 'Employee' class
    public class SalesPerson : Employee
    {
        //TODO: Define private integer field: 'percent'
        private readonly int percent;

        //TODO: Define constructor with three parameters: 'name'(string), 'salary'(decimal) and 'percent'(int). Assign two first parameters to base class.
        public SalesPerson(string name,decimal salary, int percent):base(name, salary)
        {
            this.percent = percent;
        }
        
        //TODO: Override public virtual method 'SetBonus', which increases bonus depending on percent
        public override void SetBonus(decimal bonus)
        {
            if(percent > 200)
            {
                base.SetBonus(bonus*3);
            }
            else if(percent > 100)
            {
                base.SetBonus(bonus*2);
            }
            else
            {
                base.SetBonus(bonus);
            }
        }


    }

}
